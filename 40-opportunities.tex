\section{FLOSS Opportunities for \\ Academic Software Engineering}
\label{sec:opportunities}

FLOSS brings multiple benefits for Software Engineering in an academic scenario,
providing benefits both for education and research.

\subsection{Using FLOSS for Software Engineering Education}
\label{sec:opportunities:education}

Both FLOSS development and development processes taught in traditional SE
textbooks address the challenges of multi-person construction and maintenance of
multi-version programs~\cite{DBLP:conf/ibm/Parnas74}, but with development
processes, work practices, and project forms that differ significantly and in
interesting ways \cite{Scacchi:2010}.
%
This raises the question of whether FLOSS development could be useful for
educational purposes in SE and computer science undergraduate courses, by
exposing students and teachers to different aspects of professional practice.
Such exposure may encompass a wide range of issues and activities that
includes problem solving, management, ethical and legal concerns, written and
oral communication, working as part of a team, and remaining current in a
rapidly changing discipline~\cite{SE2004}.

This issue has been exploited in different international forums 
\cite{Wolf:2002,OHara:2003,Patterson:2006,Rajendra:2006,Nandigam:2008,Hislop:2009}.
The perspective of using FLOSS for educational purposes has brought a series 
of interesting opportunities and challenges
into the Software Engineering Education agenda, such as:

\begin{itemize}
  \item
    Involving students and faculty in large-scale FLOSS projects to provide them
    with real-world experience and an understanding of the issues found in
    large, complex software projects \cite{Hislop:2009}, \cite{Patterson:2006}; 
  \item
    Reinvigorating the CS/SE curriculum and faculty members
    \cite{Patterson:2006}.  FLOSS promotes project and problem-based learning
    in which developers work on projects that interest them; by working on
    interesting and meaningful projects, they can learn concepts, skills, and
    aptitudes \cite{Faber:2002};
  \item
    Exploiting successful open source projects, in which software is highly
    modular and APIs are well documented, for teaching principles and good
    practices \cite{Nandigam:2008};
  \item
    Providing quantitative data from real, open and freely available source code
    on which to perform analysis and base decisions \cite{SE2004}.
\end{itemize}

Evidently, the use of FLOSS also brings difficulties, such as:

\begin{itemize}
    \item Because of its intrinsic characteristics, FLOSS development is not
    tipically representative of many traditional development methodologies.
    Therefore, its usefulness in SE education is restricted to only part of SE
    topics or to methodologies related to common FLOSS development processes;
    \item Academic experience on FLOSS may be seen as less important for the
    preparation of professionals in environments where proprietary software
    is the norm, especially if it would result in less academic experience with
    other widespread technologies;
    \item Involving teachers and students in FLOSS-related studies brings
    specific difficulties, for example finding projects that gave appropriate
    size and complexity (not big enough to be unbearable and not small enough to
    be trivial), allow exploring the required course topics and have enough
    documentation for starters.
\end{itemize}

%------------------------------------------------------------------------------%

\subsection{Using data from FLOSS projects in Software Engineering Research}
\label{sec:opportunities:data}

% Information is readily available
While data from private software development is often scarce and may impose high
acquisition costs, FLOSS project data is plenty and readily available for free
to anyone with an Internet connection. Private data usually cannot be shared for
replicating research results due to issues such as confidentiality requirements
and non-disclosure agreements, whereas FLOSS data is already public anyway
\cite{scacchi2007}.

% Availability of a large ammount of data
FLOSS projects can serve as an abundant source of data for Software Engineering
research. Hundreds of thousands of projects make available their source code,
which can be analyzed either manually or by automated tools. For example, in a
recent work, we studied 6773 projects using automated tools and showed how the
source code metrics affected the attractiveness of the projects, in terms of
number of downloads, page hits, and project members \cite{Meirelles:sbes2010}.
%
In another study, our group analyzed the impact of changes in the licenses of
756 FLOSS projects over 44 months to show how these changes affect the
attractiveness of these projects \cite{SantosJr:cscw2011}.
%
In yet another study we analyzed the full history of 7 FLOSS web server
projects, containing 13553 software changes, in order to investigate developers'
level of participation in the project as a predictor for variations in
structural complexity \cite{terceiro2010:core-periphery}.

Most FLOSS projects have all their communication media and development resources
-- version control system, bug tracking system, mailing lists, documentation
websites -- publicly available on the Internet, and through them researchers can
monitor, track and analyze the activity of the development group working in a
certain software artifact. Such data is being used extensively in
contemporary Software Engineering research, and systems like bicho \cite{Bicho},
Analizo \cite{terceiro2010:analizo} and others can help automate the acquisition
and analysis of this kind of data from publicly accessible repositories on the Web.

Again, it should be noted that FLOSS does not cover all possible data relevant
for SE; some methodologies and topics cannot be assessed by means of FLOSS data
analysis.
%
FLOSS-based research, however, is not limited to the previously existing
software engineering research sub-areas: studying FLOSS in itself is also a
promising research area. FLOSS development groups have been producing large and
complex products using methodologies and processes that are sometimes
substantially different from ``conventional'' software development.
Understanding how FLOSS works and its differences and similarities with regard
to ``conventional'' software engineering practices can help researchers and
practitioners to enhance the software development practice in general.

Research on FLOSS development has brought a series of interesting topics into
the Software Engineering research agenda, such as
%
the social structure of development communities
\cite{mockus2002,crowston2006,jensen2005,jensen2007},
%
communication and work flow patterns in FLOSS projects,
\cite{masmoudi2009,scialdone2009},
%
developer evolution and participation
\cite{costa2009:transflow,terceiro2010:core-periphery},
%
and attractiveness of FLOSS projects
\cite{SantosJr:cscw2011,Meirelles:sbes2010}.

%------------------------------------------------------------------------------%

\subsection{FLOSS software products in research activities}
\label{sec:opportunities:using-and-releasing-floss}

This opportunity applies not only to Software Engineering research, but also to
Computer Science research in general, as well as to any research in which there
is software development. Both researchers and society can benefit from the
interplay between FLOSS and research activity.

% Reproducibility
Releasing the source of software produced in research activities meets the basic
scientific principle of reproducibility. The scientific method demands that,
when describing a scientific achievement in a paper, researchers must provide
all the details required for an independent group of scientists to reproduce the
experiment to validate it, checking whether or not the same results are
achieved. When software plays an important role in a scientific study, either as
subject under study or as a data analysis tool, making it publicly available
under a FLOSS license will facilitate the results to be expanded and built upon.

Another benefit of releasing research-originated software as FLOSS is
fostering technology transfer from research institutions to society: being
freely licensed allows research prototypes to be enhanced into production-class
products without the researchers having to be (necessarily) involved
indefinitely.

There are several examples of successful FLOSS products that were initially
developed in the context of research activities: the PostgreSQL relational
database management system and the BSD family of operating systems were created
at the University of California, Berkeley; the Xen virtualization technology was
initially developed at the University of Cambridge; the Ginga platform for
digital television was developed in PUC-Rio and Federal University of Paraíba
and later chosen as the official standard for Brazilian National Digital
Television system. It's hard to imagine the consequences for the software
technology landscape if these projects have been kept as private research
prototypes.

Technology transfer also happens the other way around: the fact that researchers
can build their work on top of a vast amount of existing software tools,
middleware, and environments enable researchers to go deeper into their research
without having to waste time, effort, and financial resources on reinventing the
wheel. In 1676, Isaac Newton stated that he had been able to see further by
standing on the shoulders of giants. FLOSS enables today's researchers to do
the same, by reusing software components that are required for their research
but that are not the core objective of the scientific advancement they seek.
Researchers may use existing FLOSS systems such as the Linux operating system, the GCC
compiler, the Eclipse framework for IDEs, or the OpenNebula Cloud Computing
middleware and test their ideas by changing just a small portion of them. This
way researchers can concentrate on the exact point they want to explore and
advance science and technology more rapidly with more reliable results.

% vim: tw=80
