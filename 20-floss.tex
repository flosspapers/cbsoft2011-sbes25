\section{About FLOSS}
\label{sec:floss}

``FLOSS'' is a broad acronym that refers to software that promotes user freedom,
does not discriminate users or uses and, at the same time, is based on a
collaborative, efficient, and open development process.
%
FLOSS basically allows users to use, study, modify, and redistribute the
software with virtually no restrictions except, sometimes, preventing users to
impose restrictions on other users.
%
In order to make this possible, access to the source code is a necessary
condition.

Typically, such software exists by means of development projects
that are centered around some publicly-accessible source code, where
both developers and users interact with each other mainly over the Internet.
%
The code is necessarily licensed under terms that comply with either the Free
Software definition \footnote{\url{http://www.gnu.org/philosophy/free-sw.html}}
or the Open Source Definition
\footnote{\url{http://www.opensource.org/docs/definition.html}}.

Most software recognized as ``Free Software'' or ``Open Source'' actually
complies with both definitions, even if their specific terms are quite
different from one another;
%
The use of a specific terminology reflects the desire to emphasize
either the ethical or the technical aspects of the same phenomena.
%
The acronym FLOSS (Free/Libre/Open-Source Software), which we use in
this text, is gaining strong acceptance, as it encompasses both major
terminologies.

\subsection{Why FLOSS?}

FLOSS offers benefits from social, ethical, technical, and economic points of view \cite{kon,raymond1999}.
%
From a social point of view, FLOSS may be one of the many elements in
easing access to technological resources to the population at large.
%
Also, the use and support of FLOSS in e-government may offer more
transparency and ease access to services and data to citizens.
%
Finally, source code access and sharing eases learning and promotes
participation in the development of software that might otherwise
impose restrictions beyond those embodied in the Law.

From an ethical point of view, if a resource may be easily shared, preventing this should only be
pursued under very specific circumstances and with very good motivation.
%
The growth of the FLOSS model in the last decades undermines arguments in favor 
of such restrictions for software, as its sharing does not prevent other benefits to society.

From the technical and economical points of view, FLOSS offers
a development model that may result in high quality code that
gets adapted quickly to different situations with lower direct costs.
%
It also leverages the development effort of different companies
or individuals into common products, reducing the duplication of effort.

\subsection{FLOSS Development Process}

From a Software Engineering point of view, the most interesting aspect of FLOSS
is its development process.
%
A FLOSS project starts when an individual developer, or an organization,
decides to make a software product publicly available on the Internet so that
it can be freely used, modified and redistributed.

\def\patchFN{%
\footnote{A patch is a file that describes the changes to
one or more files, normally of textual content, by describing which
lines to remove and which lines to add. After receiving a patch the
developer can reproduce the changes proposed by the sender in his/her
own copy of the source code.}
}

After an initial version is released and advertised in the appropriate
communication channels, early adopters will start using the product.
Some of these early adopters may be software developers themselves,
who will be able to review the source code and propose changes that fix
defects for their specific environments or add features for their own
use cases.
%
These changes may be sent back to the original developer(s) in the form of
patches\patchFN. The project leader(s) will review the proposed changes and
apply them (or not) to the official version, so that when a new release is
made, end users will have access to these new functionalities or bug fixes.

In the course of time, each release of the product may have more features and
be more portable than its predecessor due to contributions from outside
developers. The most frequent contributors may gain trust from the initial
developer(s), and receive direct write access to the official source code of
the project, becoming able to make changes directly to the official version
of the product.

This development process is often driven by means of a version control
system (VCS).
% as illustrated in figure \ref{fig:background:free-software-repository}.
While the repository is often publicly available for read access, write
access is restricted to a limited group of developers. Other developers
will need their patches to be reviewed by a developer with the needed
privileges in order to get their contributions into the project's
official repository.

%\begin{figure}[hbtp]
  %\begin{center}
    %\includegraphics[width=0.3\textwidth]{repository-interaction.eps}
    %\caption{FLOSS development by means of a VCS repository.}
    %\label{fig:background:free-software-repository}
  %\end{center}
%\end{figure}

Besides the version control system, most FLOSS projects use a fairly
standardized environment for collaboration between their developers: an
issue tracker (or bug tracker) for organizing pending, ongoing and
future activities; one or more mailing lists for coordination and
communication between the team; and usually a web-based content
management system (such as a wiki) for community-driven documentation
writing.

The process of developers joining FLOSS projects and gaining
responsibility may range from very informal to very formal, depending on
the project. Small projects normally have informal procedures for that,
e.g.  one existing developer just offers an account in the version
control system to the new contributor. Larger projects, on the other
hand, may have more ``bureaucratic'' processes for accepting a new
developer with privileged access to the project's resources. This
``bureaucracy'' may involve filling forms with an application, signing
terms of copyright transference and other procedures.
\cite{jensen2005,jensen2007}

The following characteristics make FLOSS projects different enough from
``conventional'' software projects, making them interesting
objects of study:

\begin{itemize}
  \item \textbf{Source code availability.} Source code of FLOSS 
    projects is always available on the Internet, since most
    projects have a publicly-accessible version control repository.
  \item \textbf{User/developer symbiosis.} In most FLOSS
    projects the developers are also users of the software, and they
    also provide requirements. Maybe because of that, several free
    software projects do not have explicit requirement documents, and
    the development proceeds at a pace adequate for the developers
    to satisfy their own needs.
  \item \textbf{Non-contractual work.} A large amount of work in FLOSS
    projects is done in a non-contractual fashion. This does
    not imply that the developers are necessarily volunteers, but only
    that there is no central management with control over all of the
    developers' activities.
  \item \textbf{Work is self-assigned.} The absense of a central central
    management with control over the contributors' activities promotes
    work self-assignment: volunteer developers tend to work on the parts of
    the project that most appeal to them, and employed developers will
    work on the parts that are of most interest to their employers.
  \item \textbf{Geographical Distribution.} In most FLOSS
    projects the developers are spread among several different locations
    in the world. In projects with high geographical dispersion,
    communication is mostly performed through electronic means.
\end{itemize}

The Software Engineering literature tends to portrait FLOSS as
a homogeneous phenomenon\cite{osterlie2007b}, but most of these
characteristics do not apply to all FLOSS projects, and some of them may
be manifested in different ways across projects.
