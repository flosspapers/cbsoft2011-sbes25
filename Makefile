NAME = free-software-se-research-brazil
TEXSRCS += $(wildcard [0-9]*.tex)
BIBTEXSRCS = references.bib
TABLES = maintrack-paper-count.ltx tools-paper-count.ltx
ANALYSIS_FILES = maintrack-relative_frequency.eps tools-relative_frequency.eps $(TABLES)
OTHER = $(ANALYSIS_FILES)
CSV = maintrack.csv tools.csv

include /usr/share/latex-mk/latex.gmk

contents.ltx: $(TEXSRCS)
	ls -1 $(TEXSRCS) | sed -e 's/.*/\\input{&}/' > $@

$(ANALYSIS_FILES): analysis.R $(CSV)
	Rscript analysis.R
	sed -i -e 's/end{tabular}/&\n\\vspace{-1em}/' $(TABLES)

$(CSV): text-analysis.stamp

text-analysis.stamp:
	ruby lib/analyzer.rb
	touch $@

edit:
	gedit Makefile *.bib *.tex &

realclean: clean
	$(RM)  *.stamp
	$(RM) $(CSV)
	$(RM) $(ANALYSIS_FILES)

rebuild:
	$(MAKE) realclean
	$(MAKE) pdf
